//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var requestjson=require('request-json');
var path = require('path');
var urlMovimientos = "https://api.mlab.com/api/1/databases/msalvatierra/collections/movimientos/?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var urlClientes = "https://api.mlab.com/api/1/databases/msalvatierra/collections/clientes/?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLlab = requestjson.createClient(urlClientes) ;
var movimientosMLlab = requestjson.createClient(urlMovimientos) ;
var bodyparser = require('body-parser');
app.use(bodyparser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);



/*======CLIENTES==========*/
app.get('/clientes', function(req,res){
  clienteMLlab.get('',function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})

app.post('/clientes', function(req,res){
  clienteMLlab.post('',req.body,function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})

app.put('/clientes', function(req,res){
  clienteMLlab.post('',req.body,function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})

/*======MOVIMIENTOS==========*/
app.get('/movimientos', function(req,res){
  movimientosMLlab.get('',function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})

app.post('/movimientos', function(req,res){
  movimientosMLlab.post('',req.body,function(err,resM,body){
    if(err){
      console.log(err);
    }else{
      res.send(body);
    }
  });
})
