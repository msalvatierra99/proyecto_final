var requestJson = require('request-json');
const config=require('../config');
const data=require('../util/util');
const url=config.urlMLAB;

//Listar usuarios
function getUsers(request,response){
    console.log("listar Usuarios");
    var client = requestJson.createClient(url);
    client.get(config.mlab_users+'?'+config.apiKey,function(err,resp,body){
      if(err){
          console.log(err);
      }else {
        response.send(body);
      }
    })
}

//Crear usuario
function createUser(request,response){
    console.log("Crear usuario"+request.body.correo);
    var client=requestJson.createClient(url);
    var newUser={
              "tipoDocumento":request.body.tipoDocumento,
              "numeroDocumento":request.body.numeroDocumento,
              "nombres":request.body.nombres,
              "apellidos":request.body.apellidos,
              "correo":request.body.correo,
              "password":request.body.password
            };
    client.post(config.mlab_users+'?'+config.apiKey,newUser,function(err,resp,body){
      if(err){
        console.log(err);
      }else {
        response.send(body);
      }
    })
}

//Modificar usuario
function updateUser(request,response){
  console.log("actualizar usuario "+request.params.id);
  var client=requestJson.createClient(url);
  var query = 'q={"numeroDocumento":"' + request.params.id + '"}&';
  var cambio = '{"$set":' + JSON.stringify(request.body) + '}';
  client.put(config.mlab_users+'?'+query+config.apiKey, JSON.parse(cambio),function(err,resp,body){
    if(err){
      console.log(err);
    }else {
      response.send(body);
    }
  })

}

//Eliminar usuario
function deleteUser(request,response){
  console.log("Eliminar usuario");
  var client=requestJson.createClient(url);
  var query = 'q={"numeroDocumento":"' + request.params.id + '"}&';

  client.get(config.mlab_users+'?'+query+config.apiKey, function(errG,respG,bodyG){
    if(bodyG.length>0){
      console.log("id a eliminar... "+bodyG[0]._id.$oid);
        client.delete(config.mlab_users+'/'+bodyG[0]._id.$oid+'?'+config.apiKey,function(err,resp,body){
            if(err){
              console.log(err);
            }else {
              response.send(body);
            }
        })
    }else{
      response.status(respG.statusCode).send(data.msgEmpty);
    }
  })
}

module.exports = {
  getUsers,
  createUser,
  updateUser,
  deleteUser
}
