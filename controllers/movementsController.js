var requestJson = require('request-json');
const config=require('../config');
const url=config.urlMLAB;


//Lista de movimientos por cuenta
function getMovements(request,response){
  console.log("Listar movimientos por cuenta");
  var client=requestJson.createClient(url);
  var query = 'q={"numeroCuenta":"' + request.params.id + '"}&';
  client.get(config.mlab_account+'?'+query+config.apiKey,function(err,req,body){
    if(err){
      console.log(err);
    }
    else {
    response.send(body);
    }
  })
}

function createMovement(request,response){
  console.log("Crear movimientos");
  var client=requestJson.createClient(url);
  var query = 'q={"numeroCuenta":"' + request.params.id + '"}&';
  client.get(config.mlab_account+'?'+query+config.apiKey,function(errG,reqG,bodyG){
      if(!errG){
          if(bodyG.length>0){
            var account=bodyG[0];
            var movLength=account.movimientos.length;
            console.log("movimientos "+movLength);
            var newOperation=7452;
            if(movLength>0){
              newOperation=account.movimientos[movLength-1].operacion+1;
            }
            var tipoOperacion=request.body.tipoOperacion;
            console.log("nuevo movimiento "+newOperation);
            console.log("Body del movimiento... "+JSON.stringify(request.body));

            var montoText=request.body.monto;
            if(tipoOperacion=="Cargo"){
              montoText="-"+montoText;
            }
            var date = new Date();
            var day = date.getDate();
            var month = date.getMonth()+1;
            var year = date.getFullYear();
            var fechaActual =day+"/"+month+"/"+year;
            var newMov = {
              "operacion" : newOperation,
              "fecha" : fechaActual,
              "tipoOperacion": tipoOperacion,
              "descripcion": request.body.descripcion,
              "impuesto": "0.0",
              "monto" : montoText
            };
            var accountTemp = bodyG[0];
            var montoOperacion = parseFloat(request.body.monto);
            accountTemp.movimientos.push(newMov);
            console.log("Saldo Anterior.. "+accountTemp.saldoDisponible);
            if(tipoOperacion=="Cargo"){
            accountTemp.saldoContable = parseFloat(accountTemp.saldoContable) - montoOperacion;
            accountTemp.saldoDisponible = parseFloat(accountTemp.saldoDisponible) - montoOperacion;
            }else{
              accountTemp.saldoContable = parseFloat(accountTemp.saldoContable) + montoOperacion;
              accountTemp.saldoDisponible = parseFloat(accountTemp.saldoDisponible) + montoOperacion;
            }
            console.log("Saldo Actual.. "+accountTemp.saldoDisponible);
            client.put(config.mlab_account+'/'+account._id.$oid+'?'+config.apiKey,accountTemp,function(err,req,body){
                 if(err){
                   console.log(err);
                 }else {
                   response.send(body);
                 }
            })
        }

      }else {
        console.log(errG);
      }

  })
}

module.exports={
  getMovements,
  createMovement
}
