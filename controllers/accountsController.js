var requestJson = require('request-json');
const config=require('../config');
const url=config.urlMLAB;

//Lista de Cuentas por cliente
function getAccounts(request,response){
  console.log("Listar cuentas por cliente");
  var client=requestJson.createClient(url);
  var query = 'q={"cliente":"' + request.params.id + '"}&';
  client.get(config.mlab_account+'?'+query+config.apiKey,function(err,req,body){
    if(err){
      console.log(err);
    }
    else {
    response.send(body);
    }
  })
}

module.exports={
getAccounts
}
