'use strict'

const express=require('express');
const userController=require('../controllers/usersController');
const movementController=require('../controllers/movementsController');
const accountController=require('../controllers/accountsController');
const accessController=require('../controllers/accessController');
const security=require('../security/security');


const api = express.Router();

//Autenticacion
api.post('/login',accessController.login);
api.post('/logout',accessController.logout);

//Mantenimiento de Usuarios
//api.get('/users',security.isAuth,userController.getUsers);
api.get('/users',userController.getUsers);
api.post('/users',userController.createUser);
api.put('/users/:id',userController.updateUser);
api.delete('/users/:id',userController.deleteUser);

//Mantenimeinto de cuentas
api.get('/accounts/:id',accountController.getAccounts);

//Mantenimiento de movimientos
api.get('/movements/:id',movementController.getMovements);
api.put('/movements/:id',movementController.createMovement);

/*


//USERS
api.get('/users',seg.isAuth,userController.getUsers);
api.get('/users/:id',seg.isAuth,userController.getUser);
api.post('/users',seg.isAuth,userController.saveUser);
api.put('/users/:id',seg.isAuth,userController.updateUser);
api.delete('/users/:id',seg.isAuth,userController.removeUser);
//LOGIN
api.post('/login',authController.login);
api.post('/logout',authController.logout);
//ACCOUNTS
api.get('/accounts/:id',seg.isAuth,accountController.getAccounts);
api.post('/accounts',seg.isAuth,accountController.saveAccount);
//MOVEMENTS
api.get('/movements/:id',seg.isAuth,movementController.getMovements);
api.put('/movements/:id',seg.isAuth,movementController.saveMovement);

//TOKEN
api.get('/seg',seg.isAuth,function (req,res){
  res.status(200).send({message:'ACCESO OK'});
});
api.get('/',function (req,res){
  res.status(200).send({message:'BIENVENIDOS A NUESTRA API'});
});
*/

module.exports=api;
